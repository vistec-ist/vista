# MongoDB
Before installation, change config in Dockerfile.

## Dockerfile
Change values in angle brackets.

```dockerfile
ENV MONGO_INITDB_ROOT_USERNAME <your database root-username>
ENV MONGO_INITDB_ROOT_PASSWORD <your database root-password>
ENV MONGO_INITDB_DATABASE <your database name>
```
Then save the file.

### Example of Dockerfile

```dockerfile
ENV MONGO_INITDB_ROOT_USERNAME myuser
ENV MONGO_INITDB_ROOT_PASSWORD password1234
ENV MONGO_INITDB_DATABASE my_database_name
```
## Installation
This installation requires [Docker](https://www.docker.com/).

| :warning:  You need to create a docker network in case of running each docker container separately with this docker command below   |
|-------------------------------------------------------------------------------------------------------------------------------------|
| docker network create -d bridge --subnet 192.168.0.0/24 --gateway 192.168.0.1 mynet                                                 |

```bash
docker build . -t vista_mongo_db
```
```bash
docker run -p 27017:27017 -d -v ./backup/db:/data/db --net=mynet --name vista_mongo_db vista_mongo_db mongod --auth
```
Check docker is running?
```bash
docker container ps -a
```
#
