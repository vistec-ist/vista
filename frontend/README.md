## Frontend project setup

### Web app/frontend project stucture
````
├── public
│   ├── favicon.ico
│   └── index.html
└── src
    ├── assets
    ├── components
    |   ├── AdminSignUp.vue
    |   ├── DataTable.vue
    |   ├── LineChart.vue
    |   ├── Login.vue
    |   └── TreatmentForm.vue
    ├── plugins
    |   └── vuetify.js
    ├── router
    |   └── index.js
    ├── store
    |   └── index.js
    ├── views
    |   ├── Home.vue
    |   ├── Registration.vue
    |   └── Transactions.vue
    ├── App.vue
    └── main.js
````

### An environment variable configuration for API endpoint

Create an environment variable file (.env)

```bash
touch .env.local
```

Set the API endpoint

```bash
VUE_APP_API_ENDPOINT=http://localhost:3000/api
```

## Installation
| :warning:  You need to create a docker network in case of running each docker container separately with this docker command below   |
|-------------------------------------------------------------------------------------------------------------------------------------|
| docker network create -d bridge --subnet 192.168.0.0/24 --gateway 192.168.0.1 mynet                                                 |

```bash
docker build -t vista_frontend .
```
```bash
docker run -d -p 80:80 --net=mynet --name vista_frontend vista_frontend
```
