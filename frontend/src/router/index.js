import Vue from "vue";
import VueRouter from "vue-router";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/Home.vue"),
    meta: {
      loginRequired: true
    }
  },
  {
    path: "/login",
    name: "Login",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ "@/components/Login.vue"),
    meta: {
      guest: true
    }
  },
  {
    path: "/adminregister",
    name: "AdminSignUp",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ "@/components/AdminSignUp.vue"),
    meta: {
      loginRequired: true,
      is_admin: true
    }
  },
  {
    path: "/register",
    name: "Registration",
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/Registration.vue"),
    meta: {
      loginRequired: true
    }
  },
  {
    path: "/transactions",
    name: "Transactions",
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/Transactions.vue"),
    meta: {
      loginRequired: true
    }
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.loginRequired)) {
    if (localStorage.getItem("jwt") == null) {
      next({
        path: "/login",
        params: { nextUrl: to.fullPath }
      });
      return;
    }

    const token_exp = JSON.parse(localStorage.getItem("jwt")).token_exp;
    const timeDiff = new Date(token_exp) - new Date();
    const isExpired = timeDiff <= 0 ? true : false;

    if (isExpired) {
      localStorage.removeItem("user");
      localStorage.removeItem("jwt");
      alert("Your session is expired, please login again");
      next({
        path: "/login",
        params: { nextUrl: to.fullPath }
      });
      return;
    }

    const user = JSON.parse(localStorage.getItem("user"));
    if (to.matched.some(record => record.meta.is_admin)) {
      if (user.role == "superadmin") {
        next();
        return;
      }
      next();
    }
  }

  if (to.matched.some(record => record.meta.guest)) {
    if (localStorage.getItem("jwt") == null) {
      next();
      return;
    }
    return next({ name: "Home" });
  }
  next();
});

export default router;
