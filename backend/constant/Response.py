import config
from flask import make_response, json

def response(status_type, data=None, headers=None, params=None):
    if params:
        msg = "`" + str(params) + "` " + config.RESPONSE_CODE[status_type]["message"]
    else:
        msg = config.RESPONSE_CODE[status_type]["message"]

    res = {
        "status": config.RESPONSE_CODE[status_type]["status"],
        "data": data,
        "message": msg
    }
    resp = make_response(json.dumps(res), config.RESPONSE_CODE[status_type]["code"])
    resp.headers['Content-Type'] = 'application/json'
    resp.headers.extend(headers or {})
    return resp