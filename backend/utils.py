import config
from passlib.context import CryptContext
pwd_context = CryptContext(
        schemes=["pbkdf2_sha256"],
        default="pbkdf2_sha256",
        pbkdf2_sha256__default_rounds=30000
)

import base64
import datetime
import shutil
import os
pjoin = os.path.join

def is_correct_format(request):
    try:
        req = request.get_json(force=True)
        return True
    except:
        return False

def is_required_params_complete(request, name_list):
    for name in name_list:
        try:
            r = request[name]
        except KeyError:
            return False, name
    return True, None

def is_exists(item, params_name, list_items):
    if not item in list_items:
        return False
    return True

def encrypt_password(password):
    return pwd_context.encrypt(password)

def check_encrypted_password(password, hashed):
    return pwd_context.verify(password, hashed)

def convert_save_to_storage(imgbase64, fpath):
    if '.' in fpath.split('/')[-1]:
        fname = fpath
    else:
        fname = fpath+'.'+config.FILE_TYPE

    imgdata = base64.b64decode(imgbase64)
    try:
        with open(fname, 'wb') as f:
            f.write(imgdata)
            return True
    except:
        return False

def save_to_storage(image, fpath):
    if '.' in fpath.split('/')[-1]:
        fname = fpath
    else:
        fname = fpath+'.'+config.FILE_TYPE
    try:
        with open(fname, 'wb') as f:
            f.write(image)
            return True
    except:
        return False

def move_file(fdir, old, new):
    try:
        shutil.move(pjoin(fdir, old), pjoin(fdir, new))
        print('moved:', pjoin(fdir, old), pjoin(fdir, new))
        return True
    except:
        return False

def validate_time(date_text):
    try:
        datetime.datetime.strptime(date_text, config.TIME_FORMAT)
        return True
    except ValueError:
        return False

def get_qr(link):
    with open(link, 'r') as open_img:
        for i in open_img:
            print('i', i)
            img_data = i.split(">")

    result = str(img_data[1] + ">")
    return result
