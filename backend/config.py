import os
from pathlib import Path

# path
BASEDIR = Path(os.path.abspath(os.path.dirname(__file__))).parent
BASEDIR = '../'
IMAGES_PATH = os.path.join(BASEDIR, 'images')
QR_PATH = os.path.join(BASEDIR, 'qr')
API_PORT = int(os.environ['API_PORT'])

# mongodb setups
MONGO_DBNAME = os.environ['DB_NAME']
MONGO_HOST = os.environ['MONGO_HOST']
MONGO_URI = 'mongodb://' + MONGO_HOST + '/' + MONGO_DBNAME
MONGO_U = os.environ['MONGO_U']
MONGO_P = os.environ['MONGO_P']
MONGO_AUTH = os.environ['MONGO_AUTH']

BASEDIR = '../'
IMAGES_PATH = os.path.join(BASEDIR, 'images')
QR_PATH = os.path.join(BASEDIR, 'qr')


# image configurations
NUM_IMAGES = 5 # number of images while registering
LIMIT_NUM_IMAGES = 10 # max number of images (including both from registeration and new adding)
FILE_TYPE = 'JPEG' # default image filetype for base64 input


# list of roles
ROLE = ['superadmin', 'doctor', 'nurse', 'general']


# records
TIME_FORMAT = '%Y-%m-%d_%H-%M'


# ERROR Message configuration
RESPONSE_CODE = {
    "SUCCESS": { 
        "code": 200,
        "status": "success",
        "message": None
    },
    "AUTHENTICATION_FAILED": {
        "code": 401,
        "status": "error",
        "message": "incorrect password"
    },
    "NOT_FOUND_ERR": {
        "code": 404,
        "status": "error",
        "message": "not found"
    },
    "WRONG_REQ_ERR": {
        "code": 412,
        "status": "error",
        "message": "no data provided in request or request format is wrong"
    },
    "PARAMS_REQUIRED_ERR": {
        "code": 422,
        "status": "error",
        "message": "mandatory params required"
    },
    "PARAMS_INVALID_ERR": {
        "code": 423,
        "status": "error",
        "message": "type, length or format incorrect"
    },
    "IMAGE_CANNOT_READ_ERR":{
        "code": 424,
        "status": "error",
        "message": "images file / base64 invalid"
    },
    "DUPLICATED_ERR":{
        "code": 425,
        "status": "error",
        "message": "duplicated (already exists in DB)"
    },
    "MONGODB_ERR":{
        "code": 426,
        "status": "error",
        "message": "mongodb server error"
    },
    "FILE_SYSTEM_ERR": {
        "code": 427,
        "status": "error",
        "message": "filesystem error (i.e. image cannot be moved or removed.)"
    },
    "INTERNAL_SERVER_ERR": {
        "code": 500,
        "status": "error",
        "message": "internal server error"
    }
}
