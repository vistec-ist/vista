from flask import Blueprint
from flask_restful import Api
from routes.Index import Index
from routes.Admin import Admin
from routes.Patient import Patient
from routes.Patients import Patients
from routes.PatientID import PatientID
from routes.Record import Record
from routes.Login import Login
from routes.Memo import Memo
from routes.Treatment import Treatment
from routes.PatientQR import PatientQR

api_bp = Blueprint('api', __name__)
api = Api(api_bp)

# Route
api.add_resource(Index, '')
api.add_resource(Login, '/login')
api.add_resource(Admin, '/admin')
api.add_resource(Patient, '/patient', '/patient/<ID>')  
api.add_resource(Patients, '/patients')  
api.add_resource(PatientID, '/patient-id')
api.add_resource(Record, '/record')
api.add_resource(Memo, '/memo')
api.add_resource(Treatment, '/treatment', '/treatment/<ID>')  
api.add_resource(PatientQR, '/patient-qr', '/patient-qr/<ID>')
