from flask import request
from flask_restful import Resource
from constant.Response import response
import sys
import config
from utils import *
import __main__ as main

class Login(Resource):

    # function for login (admin)
    def post(self):
        try:
            # check request
            if is_correct_format(request):
                req = request.get_json(force=True)
            else:
                return response('WRONG_REQ_ERR')
            
            check_required, p = is_required_params_complete(req, ['username', 'password'])
            if not check_required:
                return response('PARAMS_REQUIRED_ERR', params=p)
            
            # check username & password in DB and get name of user
            user = main.mongo.db.admin.find_one({'username': req['username']})
            if user:
                if check_encrypted_password(req['password'], user['password']):
                    return response('SUCCESS', data={'name': user['name'], 'role': user['role']})
                else:
                    return response('AUTHENTICATION_FAILED')
            else:
                return response('NOT_FOUND_ERR', params='username')
        except Exception as e:
            print(e)
            return response('INTERNAL_SERVER_ERR', params=e)
