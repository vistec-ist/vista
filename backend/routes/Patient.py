from flask import request
from flask_restful import Resource
from constant.Response import response
import sys, os
import config
from utils import *
pjoin = os.path.join
import __main__ as main
import shutil
import qrcode
import qrcode.image.svg

FACTORY = qrcode.image.svg.SvgPathImage


class Patient(Resource):

    # function for register new patient
    def post(self):
        try:
            # register patients for 1st time
            required_form_params_list = ['ID']
            req_form = dict()

            # check request format
            for params in required_form_params_list:
                try:
                    data = request.form.getlist(params)
                    if (len(data) == 0) or (len(data)==1 and len(data[0])==0):
                        return response('PARAMS_REQUIRED_ERR', params=params)
                    elif (len(data) > 1):
                        return response('WRONG_REQ_ERR', params=params)
                    else:
                        req_form[params] = data[0]

                except:
                    return response('WRONG_REQ_ERR', params=params)


            # check whether this ID is exists
            if main.mongo.db.patient.find({'ID': req_form['ID']}).count() > 0:
                return response('DUPLICATED_ERR', params='ID')

            try:
                main.mongo.db.patient.insert_one(req_form)
            except Exception as e:
                return response('MONGODB_ERR', param=e)
            print(req_form)

            # generate QR
            link = pjoin(config.QR_PATH, req_form['ID']+".svg")
            img = qrcode.make(link, image_factory=FACTORY)
            img.save(link)
            print('saved QR to:', link)

            results = get_qr(link)
            return response('SUCCESS', data={'qr': results})
        
        except Exception as e:
            print(e)
            return response('INTERNAL_SERVER_ERR', params=e)



    # function for getting records
    def get(self, ID=None):
        try:
            if ID == None:
                return response('PARAMS_REQUIRED_ERR', params='ID')

            if type(ID) != str:
                return response('PARAMS_INVALID_ERR', params='ID')

            # check if ID exists in DB
            patient = main.mongo.db.patient.find_one({'ID': ID},{'_id':0})
            if patient == None:
                return response('NOT_FOUND_ERR', params='ID')

            # format profile data from DB
            results = {}

            # format records data from DB
            tmp = main.mongo.db.medical_tx.find({})
            print('tmp', list(tmp))

            records = main.mongo.db.medical_tx.find_one({'ID': ID}, {'_id':0, 'ID':0})
            if records == None:
                medical = {}
            else:
                print('records:', records)
                for med in records:
                    records[med].sort(key=lambda r: r['timestamp'])
                results['medical'] = records

            # get memo
            memo_trans = main.mongo.db.memo.find({'ID': ID}, {'_id':0, 'ID':0})
            memo = list(memo_trans)
            memo.sort(key=lambda r: r['timestamp'])
            results['memo'] = memo

            # get QR
            link = pjoin(config.QR_PATH, ID+".svg")
            if os.path.exists(link):
                results['qr'] = get_qr(link)
            else:
                results['qr'] = None

            # return response with data 
            return response('SUCCESS', data=results)
            
        except Exception as e:
            print(e)
            return response('INTERNAL_SERVER_ERR', params=e)


    # function for deleting patients
    def delete(self):

        try:
            # check request
            if is_correct_format(request):
                req = request.get_json(force=True)
            else:
                return response('WRONG_REQ_ERR')
            
            check_required, p = is_required_params_complete(req, ['ID'])
            if not check_required:
                return response('PARAMS_REQUIRED_ERR', params=p)
            
            try:
                user = main.mongo.db.patient.find_one_and_delete({'ID': req['ID']})
                if user:
                    print('Deleted:', user)
                else:
                    return response('NOT_FOUND_ERR', params='ID')

            except:
                return response('MONGODB_ERR')

            return response('SUCCESS')

        except Exception as e:
            print(e)
            return response('INTERNAL_SERVER_ERR', params=e)