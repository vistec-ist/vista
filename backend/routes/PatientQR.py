from flask import request
from flask_restful import Resource
from constant.Response import response
import sys, os
import config
from utils import *
pjoin = os.path.join
import __main__ as main
import shutil
import qrcode
import qrcode.image.svg

FACTORY = qrcode.image.svg.SvgPathImage

class PatientQR(Resource):
    # Todo: save generated image to db.
    """ Need "qrcode" library
        1. QRGenerator(id="patient_id")
        2. QRGenerator.generate()
        3. QRGenerator.get_path()
    """

    def post(self):
        try:
            if is_correct_format(request):
                req = request.get_json(force=True)
            else:
                return response('WRONG_REQ_ERR')
            
            check_required, p = is_required_params_complete(req, ['ID'])
            if not check_required:
                return response('PARAMS_REQUIRED_ERR', params=p)

            link = pjoin(config.QR_PATH, req['ID'])
            img = qrcode.make(link, image_factory=FACTORY)
            img.save(link + ".svg")
            print('saved QR to:', link)

            return response('SUCCESS')
        except Exception as e:
            print(e)
            return response('INTERNAL_SERVER_ERR', params=e)

    def get(self, ID=None):
        # This function return only <path> tag
        # This path must be inside <svg> tag
        try:
            if ID == None:
                return response('PARAMS_REQUIRED_ERR', params='ID')

            if type(ID) != str:
                return response('PARAMS_INVALID_ERR', params='ID')

            # check if ID exists in DB
            link = pjoin(config.QR_PATH, ID+".svg")
            if not os.path.exists(link):
                return response('NOT_FOUND_ERR', params='ID')

            with open(link, 'r') as open_img:
                for i in open_img:
                    print('i', i)
                    img_data = i.split(">")

            result = str(img_data[1] + ">")
            return response('SUCCESS', data={'qr': result})

        except Exception as e:
            print(e)
            return response('INTERNAL_SERVER_ERR', params=e)
