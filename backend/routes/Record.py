from flask import request
from flask_restful import Resource
from constant.Response import response
import sys, os
import config
from utils import *
import __main__ as main
pjoin = os.path.join

class Record(Resource):

    # function for putting a new record
    def post(self):
        try:
            # check request
            if is_correct_format(request):
                req = request.get_json(force=True)
            else:
                return response('WRONG_REQ_ERR')
            
            check_required, p = is_required_params_complete(req, ['ID', 'record'])
            if not check_required:
                return response('PARAMS_REQUIRED_ERR', params=p)

            if not isinstance(req['record'],dict):
                return response('PARAMS_INVALID_ERR', params='record')

            check_required, p = is_required_params_complete(req['record'], ['timestamp', 'type', 'value'])
            if not check_required:
                return response('PARAMS_REQUIRED_ERR', params=p)

            #check datetime format
            record = req['record']
            if not validate_time(record['timestamp']):
                return response('PARAMS_INVALID_ERR', params='timestamp')

            #check type format
            if type(record['type']) != str or len(record['type']) == 0:
                return response('PARAMS_INVALID_ERR', params='type')

            #check value format
            if len(str(record['value'])) == 0 or record['value'] == None:
                return response('PARAMS_INVALID_ERR', params='value')

            pid = req['ID']
            # check if ID exists in DB
            user = main.mongo.db.patient.find_one({'ID': pid})
            
            if user:
                # use exists in `patient`
                # get record of this patient
                print('user:', user)
                user = main.mongo.db.medical_tx.find_one({'ID': pid})
                rec = {"timestamp": record['timestamp'], "value": record['value']}

                if user:
                    # user exists in `medical_tx`
                    print('user', user)
                    if record['type'] in user:
                        t = user[record['type']]
                        print('Found existing record of this patient:', t)

                        for each_t in t:
                            if record['timestamp'] == each_t['timestamp']:
                                return response('DUPLICATED_ERR', params='timestamp of '+record['type'])

                        t.append(rec)
                        try:
                            to_update = {record['type']: t}
                            main.mongo.db.medical_tx.update_one({"ID": pid}, {"$set": to_update})
                            print('updated new record to db:', to_update)
                            return response('SUCCESS')
                        except Exception as e:
                            return response('MONGODB_ERR', params=e)

                    else:
                        t = [rec]
                        to_update = {record['type']: t}
                        try:
                            main.mongo.db.medical_tx.update_one({"ID": pid}, {"$set": to_update})
                            print('updated new medical value to db:', to_update)
                            return response('SUCCESS')
                        except Exception as e:
                            return response('MONGODB_ERR', params=e)

                else:
                    t = [rec]
                    print('No existing record of this patient found:', t)
                    try:
                        to_save = {"ID": pid, record['type']: t}
                        main.mongo.db.medical_tx.insert(to_save)
                        print('saved to db:', to_save)
                        return response('SUCCESS')
                    except Exception as e:
                        return response('MONGODB_ERR', params=e)

                print("record:", t)
                
            else:
                return response('NOT_FOUND_ERR', params='ID')
            

        except Exception as e:
            print(e)
            return response('INTERNAL_SERVER_ERR', e)


    
    # function for modifying an existing record
    def put(self):
        try:
            # check request
            if is_correct_format(request):
                req = request.get_json(force=True)
            else:
                return response('WRONG_REQ_ERR')
            
            check_required, p = is_required_params_complete(req, ['ID', 'record'])
            if not check_required:
                return response('PARAMS_REQUIRED_ERR', params=p)

            if not isinstance(req['record'],dict):
                return response('PARAMS_INVALID_ERR', params='record')

            check_required, p = is_required_params_complete(req['record'], ['timestamp', 'type', 'value'])
            if not check_required:
                return response('PARAMS_REQUIRED_ERR', params=p)

            #check datetime format
            record = req['record']
            if not validate_time(record['timestamp']):
                return response('PARAMS_INVALID_ERR', params='timestamp')

            #check type format
            if type(record['type']) != str or len(record['type']) == 0:
                return response('PARAMS_INVALID_ERR', params='type')

            #check value format
            if len(str(record['value'])) == 0 or record['value'] == None:
                return response('PARAMS_INVALID_ERR', params='value')

            pid = req['ID']
            # check if ID exists in DB
            user = main.mongo.db.patient.find_one({'ID': pid})
            
            if user:
                # use exists in `patient`
                # get record of this patient
                print('user:', user)
                user = main.mongo.db.medical_tx.find_one({'ID': pid})
                rec = {"timestamp": record['timestamp'], "value": record['value']}

                if user:
                    # user exists in `medical_tx`
                    print('user', user)
                    if record['type'] in user:
                        t = user[record['type']]
                        print('Found existing record of this patient:', t)

                        found = False
                        for each_t in t:
                            if record['timestamp'] == each_t['timestamp']:
                                each_t['value'] = record['value']
                                print('change record to:', each_t)
                                found = True

                        if not found:
                            return response('NOT_FOUND_ERR', params='record')

                        try:
                            to_update = {record['type']: t}
                            main.mongo.db.medical_tx.update_one({"ID": pid}, {"$set": to_update})
                            print('updated new record to db:', to_update)
                            return response('SUCCESS')
                        except Exception as e:
                            return response('MONGODB_ERR', params=e)

                    else:
                        return response('NOT_FOUND_ERR', params='type')

                else:
                    return response('NOT_FOUND_ERR', params='record')

                print("record:", t)
                
            else:
                return response('NOT_FOUND_ERR', params='ID')

        except Exception as e:
            print(e)
            return response('INTERNAL_SERVER_ERR', e)


    # function for delete an existing record
    def delete(self):
        try:
            # check request
            if is_correct_format(request):
                req = request.get_json(force=True)
            else:
                return response('WRONG_REQ_ERR')
            
            check_required, p = is_required_params_complete(req, ['ID', 'record'])
            if not check_required:
                return response('PARAMS_REQUIRED_ERR', params=p)

            if not isinstance(req['record'],dict):
                return response('PARAMS_INVALID_ERR', params='record')

            check_required, p = is_required_params_complete(req['record'], ['timestamp', 'type'])
            if not check_required:
                return response('PARAMS_REQUIRED_ERR', params=p)

            #check datetime format
            record = req['record']
            if not validate_time(record['timestamp']):
                return response('PARAMS_INVALID_ERR', params='timestamp')

            #check type format
            if type(record['type']) != str or len(record['type']) == 0:
                return response('PARAMS_INVALID_ERR', params='type')

            pid = req['ID']
            # check if ID exists in DB
            user = main.mongo.db.patient.find_one({'ID': pid})
            
            if user:
                # use exists in `patient`
                # get record of this patient
                print('user:', user)
                user = main.mongo.db.medical_tx.find_one({'ID': pid})

                if user:
                    # user exists in `medical_tx`
                    if record['type'] in user:
                        t = user[record['type']]
                        print('Found existing record of this patient:', t)

                        record_index = None
                        for i, each_t in enumerate(t):
                            if record['timestamp'] == each_t['timestamp']:
                                record_index = i

                        if record_index == None:
                            return response('NOT_FOUND_ERR', params='record')
                        
                        del t[record_index]
                        try:
                            to_update = {record['type']: t}
                            main.mongo.db.medical_tx.update_one({"ID": pid}, {"$set": to_update})
                            print('updated new record to db:', to_update)
                            return response('SUCCESS')
                        except Exception as e:
                            return response('MONGODB_ERR', params=e)

                    else:
                        return response('NOT_FOUND_ERR', params='type')

                else:
                    return response('NOT_FOUND_ERR', params='record')

                print("record:", t)
                
            else:
                return response('NOT_FOUND_ERR', params='ID')

        except Exception as e:
            print(e)
            return response('INTERNAL_SERVER_ERR', e)