from flask import request
from flask_restful import Resource
from constant.Response import response
import sys, os
import config
from utils import *
pjoin = os.path.join

class PatientID(Resource):

    def post(self):
        # for querying patient's data from an input image
        try:
            req = request.get_json(force=True)
        except:
            return response('WRONG_REQ_ERR', params='image')
        
        try:
            img = req['image']
        except KeyError:
            return response('PARAMS_REQUIRED_ERR', params='image')

        # TODO: embedded from img

        # TODO: similarity search from DB

        # return patient's ID

        return response('SUCCESS')
