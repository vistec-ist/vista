from flask import request
from flask_restful import Resource
from constant.Response import response
import sys, os
import config
from utils import *
import __main__ as main
pjoin = os.path.join

class Treatment(Resource):

    # function for putting a treatment record
    def post(self):
        try:
            # check request
            if is_correct_format(request):
                req = request.get_json(force=True)
            else:
                return response('WRONG_REQ_ERR')
            
            check_required, p = is_required_params_complete(req, ['ID', 'timestamp', 'record', 'name'])
            if not check_required:
                return response('PARAMS_REQUIRED_ERR', params=p)

            #check datetime format
            if not validate_time(req['timestamp']):
                return response('PARAMS_INVALID_ERR', params='timestamp')

            #check record format
            print(type(req['record']))
            if type(req['record']) != dict:
                return response('PARAMS_INVALID_ERR', params='record')

            pid = req['ID']
            # check if ID exists in DB
            user = main.mongo.db.patient.find_one({'ID': req['ID']})
            if user:
                # check existing timestamp
                if main.mongo.db.treatment.find({'ID': req['ID'], 'timestamp': req['timestamp']}).count() > 0:
                    return response('DUPLICATED_ERR', params='timestamp')

                try:
                    # add record object to DB
                    main.mongo.db.treatment.insert(req)
                    print('saved to db:', req)
                except Exception as e:
                    return response('MONGODB_ERR', params=e)
            else:
                return response('NOT_FOUND_ERR', params='ID')

            return response('SUCCESS')
        except Exception as e:
            print(e)
            return response('INTERNAL_SERVER_ERR', e)


    # function for getting records
    def get(self, ID=None):
        try:
            if ID == None:
                return response('PARAMS_REQUIRED_ERR', params='ID')

            if type(ID) != str:
                return response('PARAMS_INVALID_ERR', params='ID')

            # check if ID exists in DB
            patient = main.mongo.db.patient.find_one({'ID': ID},{'_id':0})
            if patient == None:
                return response('NOT_FOUND_ERR', params='ID')

            # format profile data from DB
            results = {}

            # get treatment record
            treatment_trans = main.mongo.db.treatment.find({'ID': ID}, {'_id':0, 'ID':0})
            treatment = list(treatment_trans)
            treatment.sort(key=lambda r: r['timestamp'])
            results['treatment'] = treatment

            # return response with data 
            return response('SUCCESS', data=results)
            
        except Exception as e:
            print(e)
            return response('INTERNAL_SERVER_ERR', params=e)