from flask import request
from flask_restful import Resource
from constant.Response import response
import sys, os
import config
from utils import *
pjoin = os.path.join
import __main__ as main
import shutil


class Patients(Resource):

    # function for getting records
    def get(self):
        try:
            # check if ID exists in DB
            patients = main.mongo.db.patient.find({},{'_id':0})
            results = []
            for patient in patients:
                if len(patient) == 1 and 'ID' in list(patient.keys()):
                    results.append(patient['ID'])
            
            # return response with data 
            return response('SUCCESS', data=results)

        except Exception as e:
            print(e)
            return response('INTERNAL_SERVER_ERR', e)
