from flask import request
from flask_restful import Resource
from constant.Response import response
import sys, os
import config
from utils import *
import __main__ as main
pjoin = os.path.join

class Memo(Resource):

    # function for putting a memo
    def post(self):
        try:
            # check request
            if is_correct_format(request):
                req = request.get_json(force=True)
            else:
                return response('WRONG_REQ_ERR')
            
            check_required, p = is_required_params_complete(req, ['ID', 'timestamp', 'message', 'name'])
            if not check_required:
                return response('PARAMS_REQUIRED_ERR', params=p)

            #check datetime format
            if not validate_time(req['timestamp']):
                return response('PARAMS_INVALID_ERR', params='timestamp')

            pid = req['ID']
            # check if ID exists in DB
            user = main.mongo.db.patient.find_one({'ID': req['ID']})
            if user:
                try:
                    # add message to DB
                    main.mongo.db.memo.insert(req)
                    print('saved to db:', req)
                except Exception as e:
                    return response('MONGODB_ERR', params=e)
            else:
                return response('NOT_FOUND_ERR', params='ID')

            return response('SUCCESS')
        except Exception as e:
            print(e)
            return response('INTERNAL_SERVER_ERR', e)