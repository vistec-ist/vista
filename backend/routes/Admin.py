from flask import request
from flask_restful import Resource
from constant.Response import response
import sys
import config
from utils import *
import __main__ as main

class Admin(Resource):

    # function for create new admin's account
    def post(self):
        try:
            # check request
            if is_correct_format(request):
                req = request.get_json(force=True)
            else:
                return response('WRONG_REQ_ERR')
            
            check_required, p = is_required_params_complete(req, ['username', 'password', 'name', 'role'])
            if check_required == True:
                req['password'] = encrypt_password(req['password'])
            else:
                return response('PARAMS_REQUIRED_ERR', params=p)
            
            if not is_exists(req['role'], 'role', config.ROLE):
                return response('NOT_FOUND_ERR', params='role')

            # check whether username exists
            user = main.mongo.db.admin.find_one({'username': req['username']})
            if user:
                return response('DUPLICATED_ERR', params='username')
            else:
                # Add req to db
                try:
                    main.mongo.db.admin.insert_one(req)
                    print('saved to db:', req)
                except Exception as e:
                    return response('MONGODB_ERR', params=e)

                return response('SUCCESS')

        except Exception as e:
            print(e)
            return response('INTERNAL_SERVER_ERR', e)
    
    def delete(self):
        try:
            # check request
            if is_correct_format(request):
                req = request.get_json(force=True)
            else:
                return response('WRONG_REQ_ERR')
            
            check_required, p = is_required_params_complete(req, ['username'])
            if not check_required:
                return response('PARAMS_REQUIRED_ERR', params=p)
            
            try:
                user = main.mongo.db.admin.find_one_and_delete({'username': req['username']})
                if user:
                    print('Deleted:', user)
                else:
                    return response('NOT_FOUND_ERR', params='username')

            except:
                return response('MONGODB_ERR')

            return response('SUCCESS')

        except Exception as e:
            print(e)
            return response('INTERNAL_SERVER_ERR', params=e)