import qrcode
import qrcode.image.svg


FACTORY = qrcode.image.svg.SvgPathImage


class QRGenerator:
    """ Need "qrcode" library
        1. qrgen = QRGenerator(id="patient_id")
        2. qrgen.generate()
        3. qrgen.get_path()
    """
    def __init__(self, id=""):
        # patient_id from 'id' param form request.
        self.id = id
        self.link = "hostIP/api/patient/" + id

    def generate(self):
        try:
            fpath = "./qrcode/" + str(self.id) + "qr.svg"
            img = qrcode.make(self.link, image_factory=FACTORY)
            img.save(fpath)
        except Exception as e:
            print(e)

    def get_path(self):
        # This function return only <path> tag
        # This path must be inside <svg> tag
        img_data = []
        try:
            fpath = "./qrcode/" + str(self.id) + "qr.svg"
            with open(fpath, 'r') as open_img:
                for i in open_img:
                    img_data = i.split(">")

            return str(img_data[1] + ">")
        except Exception as e:
            print(e)
            return "error"
