from flask import Flask
import os
import config
from flask_pymongo import PyMongo
from flask_pymongo import MongoClient
from flask_cors import CORS
from flask_swagger_ui import get_swaggerui_blueprint

def create_app(config_filename):
    app = Flask(__name__)
    app.config.from_object(config_filename)
    # app.config['MONGO_URI'] = "mongodb://"+config.MONGO_U+":"+config.MONGO_P+"@"+config.MONGO_HOST+"/"+config.MONGO_DBNAME
    app.config['MONGO_URI'] = "mongodb://"+config.MONGO_U+":"+config.MONGO_P+"@"+config.MONGO_HOST+"/"+config.MONGO_DBNAME+"?authSource="+config.MONGO_AUTH

    SWAGGER_URL = '/api/docs'
    API_URL = '/static/swagger.yaml'
    SWAGGERUI_BLUEPRINT = get_swaggerui_blueprint(
        SWAGGER_URL,
        API_URL,
        config={
            'app_name': "VISTA"
        }
    )
    app.register_blueprint(SWAGGERUI_BLUEPRINT, url_prefix=SWAGGER_URL)

    mongo = PyMongo(app)
    CORS(app, resources={r"/api/*": {"origins": "*"}}, supports_credentials=True)

    try:
        client = MongoClient(app.config['MONGO_URI'], serverSelectionTimeoutMS = 2000)
        client.server_info()
        print("---------------- DB connected ----------------")
    except:
        print("Fail connect DB")
    
    from app import api_bp
    app.url_map.strict_slashes = False
    app.register_blueprint(api_bp, url_prefix='/api/')

    return app, mongo


class DB:

    @staticmethod
    def setup(mongo):
        mongo = mongo

if __name__ == "__main__":
    if not os.path.exists(config.IMAGES_PATH):
        os.mkdir(config.IMAGES_PATH)
    
    if not os.path.exists(config.QR_PATH):
        os.mkdir(config.QR_PATH)

    app, mongo = create_app("config")
    app.run(host='0.0.0.0', port=config.API_PORT, debug=True)