<h3 align="center">Partners</h3>

<div align="center">
    <img width="200px" src="https://www.vistec.ac.th/img/logo.png">
    <img width="150px" src="https://pbs.twimg.com/profile_images/1224575089812729856/LQNOCKUy_400x400.jpg">
    <img width="230px" src="https://upload.wikimedia.org/wikipedia/en/thumb/4/47/PTT_Global_Chemical_logo.svg/330px-PTT_Global_Chemical_logo.svg.png">
    <img width="100px" src="https://upload.wikimedia.org/wikipedia/en/f/f5/Emblem_of_Queen_Sirikit_Naval_Hospital.png">
</div>

## Introduction
VISTA (Vital-sign data platform) เป็นเว็บแอปพลิเคชันที่ใช้ช่วยในการสื่อสารระหว่างพยาบาลภายในห้องผู้ป่วยแยก (isolation ward) กับแพทย์ที่ไม่สามารถเข้าไปยังห้องผู้ป่วยแยกได้ โดยปกติแล้วพยาบาลจำเป็นต้องวัดสัญญาณชีพของคนไข้เป็นประจำอย่างน้อยวันละ 2 ถึง 3 ครั้ง เพื่อสังเกตการณ์อาการของผู้ป่วย ข้อมูลเหล่านี้จะถูกส่งต่อไปให้แพทย์ โดยผ่านช่องทางการสื่อสารทางไกลชนิดต่างๆ เช่นทางแอปพลิเคชันไลน์เป็นต้น

VISTA ถูกสร้างขึ้นมาเพื่อเพิ่มความสะดวกในการสื่อสารระหว่างพยาบาลและแพทย์โดยผนวกการแสดงข้อมูลด้วยกราฟเข้าไปในช่องทางการสื่อสาร ซึ่งจะช่วยให้แพทย์วิเคราะห์สัญญาณชีพได้ดีมากยิ่งขึ้น โปรเจคนี้เป็นโครงการการกุศลที่ VISTEC มีแผนที่จะทำการเผยแพร่ซอร์สโค้ด (Open source) ระบบนี้เพื่อให้โรงพยาบาลหรือบุคคลทั่วไปนำไปใช้และพัฒนาต่อยอดโดยไม่เสียค่าใช้จ่าย

VISTA เป็นส่วนนึงของโครงการความร่วมมือระหว่าง PTT-GC, ทีม IRAPs จากมหาวิทยาลัยเทคโนโลยีพระจอมเกล้าพระนครเหนือ และโรงพยาบาลสมเด็จพระนางเจ้าสิริกิติ์ กรมแพทย์ทหารเรือ โดย PTT-GC บริจาคทุนทรัพย์, ทีม IRAPs สร้างหุ่นยนต์วัดไข้ และโรงพยาบาลสมเด็จพระนางเจ้าสิริกิติ์ให้ความรู้และพื้นที่ในการทดลองและพัฒนา

## Used frameworks & tools
- `Vue.js`
- `Vuetify`
- `Bootstrap`
- `Docker`
- `Flask`
- `MongoDB`
- `Nginx`

### Project structure
````
├── vista
    ├── frontend
    ├── backend
    └── db
````

## Installation

### Web app/frontend [vista/frontend]
- [Documentation](frontend/README.md)

### Backend (API) [vista/backend]
- [Documentation](backend/README.md)

### Backend (database) [vista/db]
- [Documentation](db/README.md)


## Browser Compatibility
Vue.js supports all browsers that are ES5-compliant (IE8 and below are not supported).

## Contribution
#### Supervisors
- Assoc. Prof. Dr. Sarana Nutanong
- Prof. Dr. Poramate Manoonpong
- Dr. Nat Dilokthanakul
- Kasidit Phoncharoen
- Benjapol Worakan

#### Developers
- Treephop Saeteng (frontend developer)
- Nannapas Banluesombatkul (backend developer)
- Rattasat Laotaew (database administrator)

## License

[CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)

Copyright (c) 2020-present, Vidyasirimedhi Institute of Science and Technology (VISTEC)